package com.tomcat;
import java.io.IOException;  
import java.util.Map;

import javax.servlet.ServletException;  
import javax.servlet.http.HttpServlet;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;  

import org.apache.catalina.Context;
public class ManagerSevlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Map<String, Context> map;
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {  
        System.out.println("request scheme: " + req.getScheme());  
        resp.getWriter().print("hello tomcat"); 
    }  
}
