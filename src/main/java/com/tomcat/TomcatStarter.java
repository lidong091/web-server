package com.tomcat;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.startup.Tomcat.FixContextListener; 

public class TomcatStarter {
	public static void main(String[] args) throws Exception {
		startup();
	}
	public static void startup() throws LifecycleException {
		Tomcat tomcat=new Tomcat();
		tomcat.setPort(8099);
		tomcat.setBaseDir(".");
		tomcat.getHost().setAutoDeploy(false);
		
		//开启https，
		//注意，在生成密钥的时候要用RSA加密，要不然只能在IE上打开,chrome（版本 53.0.2785.116）会禁止打开
//		Connector connector = new Connector(DEFAULT_PROTOCOL);
//        connector.setPort(ports);
//        
//        Http11NioProtocol protocol = (Http11NioProtocol)connector.getProtocolHandler();
//        protocol.setKeystorePass("123456");
//        protocol.setKeystoreFile("e:/tmp/ssl/boot.keystore");
//        protocol.setKeyAlias("mykey");
//        protocol.setSSLEnabled(true);
//        
//        tomcat.getService().addConnector(connector);
		
		Context ctx=new StandardContext();
		ctx.setPath("");
		ctx.addLifecycleListener(new FixContextListener());
		Tomcat.addServlet(ctx, "homeServlet", new ManagerSevlet());  
		ctx.addServletMappingDecoded("/home", "homeServlet");  
		tomcat.getHost().addChild(ctx);
		tomcat.start();
		tomcat.getServer().await();
	}
}
