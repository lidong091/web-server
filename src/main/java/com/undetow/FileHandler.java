package com.undetow;

import java.io.File;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.form.FormParserFactory;
import io.undertow.util.AttachmentKey;
import io.undertow.util.Headers;
import io.undertow.util.MultipartParser;

public class FileHandler implements HttpHandler {
	//File.listRoots()
	@Override
    public void handleRequest(final HttpServerExchange exchange) throws Exception {
		String auth=exchange.getRequestHeaders().get("Authorization", 0);
		if("Basic bGlkb25nOjEyMzQ1Ng==".equals(auth)) {
			exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html;charset=UTF-8");
			File[] files=File.listRoots();
			String param=exchange.getQueryParameters().toString();
			FormDataParser form = FormParserFactory.builder().build().createParser(exchange);
			if(form!=null) {
//				form.setCharacterEncoding("UTF-8");
//				FormData data = form.parseBlocking();
				form.parseBlocking().get("11").getLast().isFile();
			}
			param+="<form method='post' enctype='multipart/form-data'><input type='file' name='test' ><input type='submit'value='提交'></form>";
			exchange.getResponseSender().send(param);
			
		}else {
			exchange.setStatusCode(401);
			exchange.getResponseHeaders().put(Headers.WWW_AUTHENTICATE, "Basic Realm=\"tomcat\"");
		}
    }
}
