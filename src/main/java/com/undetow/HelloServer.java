package com.undetow;

import java.io.File;
import java.nio.file.Paths;

import io.undertow.Undertow;
import io.undertow.server.handlers.resource.PathResourceManager;
import static io.undertow.Handlers.resource;
public class HelloServer {
	public static void main(final String[] args) {
        Undertow server = Undertow.builder().addHttpListener(8080, "localhost")
        		.setHandler(new FileHandler()).build();
//        Undertow server = Undertow.builder()
//                .addHttpListener(8080, "localhost")
//                .setHandler(resource(new PathResourceManager(Paths.get(System.getProperty("user.home")), 100))
//                        .setDirectoryListingEnabled(true))
//                .build();
        server.start();
    }
}
